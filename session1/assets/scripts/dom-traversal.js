const mainDiv = document.getElementById('root');

// console.log(mainDiv.children[1].children[1].children);

// console.log(mainDiv.firstChild);

// console.log(mainDiv.lastChild);

//first child and last child will output the nodes, not the element nodes.

// console.log(mainDiv.firstElementChild);

// console.log(mainDiv.lastElementChild.lastElementChild.firstElementChild);

//first element child and last element child will output the element, disregarding the text nodes

// console.log(mainDiv.lastElementChild.parentElement);
// parentElement traverses back to the parent element

// nextElementSibling and previousElementSibling
// console.log(mainDiv.firstElementChild.nextElementSibling.lastElementChild.firstElementChild.nextElementSibling); for dragon fruit


// console.log(mainDiv.firstElementChild.nextElementSibling.lastElementChild.lastElementChild); starfruit

console.log(mainDiv.lastElementChild.lastElementChild.lastElementChild);