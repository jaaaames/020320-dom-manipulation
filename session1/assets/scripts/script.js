// console.log(document.body);

// getElementById

// let h1 = document.getElementById('main-title');

// let stud2 = document.getElementById('stud-2');

// console.log(h1);
// console.log(stud2);

// getElementsByTagName

// let header = document.getElementsByTagName('h1');

// console.log(header);

// let studs = document.getElementsByTagName('li');

// console.log(studs);

// console.log(studs[2]);

// console.log(studs[0].children);

// getElementsByClassName;

// let studlist = document.getElementsByClassName('studlist');

// console.log(studlist[0].children[1].textContent);

// querySelector
// let head1 = document.querySelectorAll('li + li');

// console.log(head1);

const h1 = document.getElementById('main-title');

h1.style.backgroundColor = 'red';
h1.style.color = 'white';
h1.style.fontSize = "50px";
h1.style.textAlign = "center";
h1.textContent = "Hello from B55";